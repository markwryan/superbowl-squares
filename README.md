# Superb Owl Squares

Randomly set squares based on a list of names and quantites from a spreadsheet.

## Usage

Export or generate the list of requests. These should be one per line in the form of `Name,Quantity`.

An example list would be
```
Mark Ryan,5
John Smith,10
Kip Kipperton,2
Biff Bigsly,4
```

Save this file as `square_requests.csv` in this folder.

Run superbowl-squares.py,

```
python superbowl-squares.py
```

Each name should output a separate text file with their squares. Additionally, 
`all.txt` will hold all the output squares per person.