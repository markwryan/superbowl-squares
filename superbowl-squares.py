import random
# Updated for 2020 SB
# Generate the 100 Squares in score form of (KC,SF)
squares = []
for i in range (10):
    for j in range(10):
        squares.append((i,j))

# Read CSV with each line being a separate Name,Number of Squares from a google doc
with open("square_requests.csv") as requests:
    for request in requests:
        r = request.split(',')
        name = r[0]
        quantity = int(r[1])
        # Write each persons squares to a separate txt file
        personal = open("{}.txt".format(name), 'a')
        # Also track all picks in a separate all.txt file.
        overall = open("all.txt", 'a')
        overall.write("----{}----\n".format(name))
        for i in range(quantity):
            # Randomly select and remove a square
            square = squares.pop(random.randint(0,len(squares)-1))
            output = "KC: {0} SF: {1}\n".format(square[0], square[1])
            personal.write(output)
            overall.write(output)
        personal.close()
        overall.close()
